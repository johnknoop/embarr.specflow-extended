﻿using System;
using Embarr.SpecFlow.Extensions.Models;
using Embarr.SpecFlow.Extensions.Services;
using Embarr.SpecFlow.UnitTests.Models;
using Moq;
using NUnit.Framework;
using Should;

namespace Embarr.SpecFlow.UnitTests.Extensions.Services
{
    [TestFixture]
    public class PropertyBuilderServiceTests
    {
        private PropertyBuilderService propertyBuilderService;
        private Mock<ISimpleTypeService> simpleTypeService;

        [SetUp]
        public void SetUp()
        {
            simpleTypeService = new Mock<ISimpleTypeService>(MockBehavior.Strict);
            propertyBuilderService = new PropertyBuilderService(simpleTypeService.Object);
        }

        public class BuildProperty : PropertyBuilderServiceTests
        {
            [Test]
            public void Should_throw_exception_if_property_does_not_exist_on_type()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property();
                property.Parts.Add(new PropertyPart
                {
                    Name = "DoesNotExistOnModel"
                });

                // Act
                var exception = Assert.Throws<ArgumentException>(() => propertyBuilderService.BuildProperty(property, 0, testModel));

                // Assert
                exception.Message.ShouldEqual("The model 'TestModel' does not appear to contain a property called 'DoesNotExistOnModel'");
            }

            [Test]
            public void Should_throw_exception_if_property_partIndex_is_greater_than_sum_of_parts_minus_1()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property();
                property.PropertyPath = "MyPropertyPath";
                property.Parts.Add(new PropertyPart
                {
                    Name = "DoesNotExistOnModel"
                });

                // Act
                var exception = Assert.Throws<ArgumentException>(() => propertyBuilderService.BuildProperty(property, 1, testModel));

                // Assert
                exception.Message.ShouldEqual("Please check '" + property.PropertyPath + "'. You might be trying to set a complex object rather than a primitive/simple item from the complex object");
            }

            [Test]
            public void Should_return_if_read_only_property()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "newValue"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "IamReadOnly"
                });

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.IamReadOnly.ShouldNotEqual(property.RawStringValue);
            }

            [Test]
            public void Should_set_simple_type()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "newValue"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleStringType"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue("newValue", typeof (string))).Returns("newValue");

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.SimpleStringType.ShouldEqual(property.RawStringValue);
            }

            [Test]
            public void Should_build_complex_type()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "myChildValue"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "Child",
                    Path = "Child"
                });
                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleStringType",
                    Path = "SimpleStringType"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue("myChildValue", typeof(string))).Returns("myChildValue");

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);
   
                // Assert
                testModel.Child.SimpleStringType.ShouldEqual(property.RawStringValue);
            }


            [Test]
            public void Should_build_array_of_simple_Types()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "mySimpleArrayType"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleTypeArray",
                    Path = "SimpleTypeArray[0]",
                    Key = "0"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.RawStringValue, typeof(string))).Returns(property.RawStringValue);

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.SimpleTypeArray.ShouldNotBeNull();
                testModel.SimpleTypeArray.Length.ShouldEqual(1);
                testModel.SimpleTypeArray[0].ShouldEqual(property.RawStringValue);
            }

            [Test]
            public void Should_build_array_of_complex_Types()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "myComplexArrayType"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "ComplexTypeArray",
                    Path = "ComplexTypeArray[0]",
                    Key = "0"
                });
                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleStringType",
                    Path = "SimpleStringType"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.RawStringValue, typeof(string))).Returns(property.RawStringValue);

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.ComplexTypeArray.ShouldNotBeNull();
                testModel.ComplexTypeArray.Length.ShouldEqual(1);
                testModel.ComplexTypeArray[0].SimpleStringType.ShouldEqual(property.RawStringValue);
            }
            
            [Test]
            public void Should_build_list_of_simple_Types()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "mySimpleListType"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleTypeList",
                    Path = "SimpleTypeList[0]",
                    Key = "0"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.RawStringValue, typeof(string))).Returns(property.RawStringValue);

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.SimpleTypeList.ShouldNotBeNull();
                testModel.SimpleTypeList.Count.ShouldEqual(1);
                testModel.SimpleTypeList[0].ShouldEqual(property.RawStringValue);
            }

            [Test]
            public void Should_build_list_of_complex_Types()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "myComplexListType"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "ComplexTypeList",
                    Path = "ComplexTypeList[0]",
                    Key = "0"
                });
                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleStringType",
                    Path = "SimpleStringType"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.RawStringValue, typeof(string))).Returns(property.RawStringValue);

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.ComplexTypeList.ShouldNotBeNull();
                testModel.ComplexTypeList.Count.ShouldEqual(1);
                testModel.ComplexTypeList[0].SimpleStringType.ShouldEqual(property.RawStringValue);
            }

            [Test]
            public void Should_build_dictionary_of_simple_Types()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "myComplexDictionaryValue"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleTypeDictionary",
                    Path = "SimpleTypeDictionary[\"myKey\"]",
                    Key = "myKey"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.Parts[0].Key, typeof(string))).Returns(property.Parts[0].Key);
                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.RawStringValue, typeof(string))).Returns(property.RawStringValue);

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.SimpleTypeDictionary.ShouldNotBeNull();
                testModel.SimpleTypeDictionary.Count.ShouldEqual(1);
                testModel.SimpleTypeDictionary["myKey"].ShouldEqual(property.RawStringValue);
            }

            [Test]
            public void Should_build_dictionary_of_complex_Types()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "mySimpleDictionaryValue"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "ComplexTypeDictionary",
                    Path = "ComplexTypeDictionary[\"myKey\"]",
                    Key = "myKey"
                });

                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleStringType",
                    Path = "SimpleStringType"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.Parts[0].Key, typeof(string))).Returns(property.Parts[0].Key);
                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.RawStringValue, typeof(string))).Returns(property.RawStringValue);

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.ComplexTypeDictionary.ShouldNotBeNull();
                testModel.ComplexTypeDictionary.Count.ShouldEqual(1);
                testModel.ComplexTypeDictionary["myKey"].SimpleStringType.ShouldEqual(property.RawStringValue);
            }

            [Test]
            public void Should_build_int_keyed_dictionary_of_complex_Types()
            {
                // Arrange
                var testModel = new TestModel();
                var property = new Property
                {
                    RawStringValue = "mySimpleDictionaryValue"
                };
                property.Parts.Add(new PropertyPart
                {
                    Name = "ComplexTypeIntKeyedDictionary",
                    Path = "ComplexTypeIntKeyedDictionary[123]",
                    Key = "123"
                });

                property.Parts.Add(new PropertyPart
                {
                    Name = "SimpleStringType",
                    Path = "SimpleStringType"
                });

                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.Parts[0].Key, typeof(int))).Returns(int.Parse(property.Parts[0].Key));
                simpleTypeService.Setup(x => x.GetSimpleTypeValue(property.RawStringValue, typeof(string))).Returns(property.RawStringValue);

                // Act
                propertyBuilderService.BuildProperty(property, 0, testModel);

                // Assert
                testModel.ComplexTypeIntKeyedDictionary.ShouldNotBeNull();
                testModel.ComplexTypeIntKeyedDictionary.Count.ShouldEqual(1);
                testModel.ComplexTypeIntKeyedDictionary[123].SimpleStringType.ShouldEqual(property.RawStringValue);
            }
        }
    }
}