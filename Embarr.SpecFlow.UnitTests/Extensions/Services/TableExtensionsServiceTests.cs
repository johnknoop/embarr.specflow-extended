﻿using System;
using System.Collections.Generic;
using Embarr.SpecFlow.Extensions.Models;
using Embarr.SpecFlow.Extensions.Services;
using Embarr.SpecFlow.UnitTests.Models;
using Moq;
using NUnit.Framework;
using Should;

namespace Embarr.SpecFlow.UnitTests.Extensions.Services
{
    [TestFixture]
    public class TableExtensionsServiceTests
    {
        private TableExtensionsService tableExtensionsService;
        private Mock<IPropertyBuilderService> propertyBuilderService;

        [SetUp]
        public void SetUp()
        {
            propertyBuilderService = new Mock<IPropertyBuilderService>(MockBehavior.Strict);
            tableExtensionsService = new TableExtensionsService(propertyBuilderService.Object);
        }

        public class CreateDeepInstance : TableExtensionsServiceTests
        {
            [Test]
            public void Should_throw_exception_if_properties_collection_is_null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => tableExtensionsService.CreateDeepInstance<string>(null));

                // Assert
                exception.Message.ShouldEqual("CreateDeepInstance properties cannot be null");
            }

            [Test]
            public void Should_create_instance_and_build_properties()
            {
                // Arrange
                var properties = new List<Property>();
                var property1 = new Property();
                var property2 = new Property();
                properties.Add(property1);
                properties.Add(property2);

                propertyBuilderService.Setup(x => x.BuildProperty(property1, 0, It.IsAny<TestModel>())).Verifiable();
                propertyBuilderService.Setup(x => x.BuildProperty(property2, 0, It.IsAny<TestModel>())).Verifiable();

                // Act
                var result = tableExtensionsService.CreateDeepInstance<TestModel>(properties);

                // Assert
                result.ShouldNotBeNull();
                propertyBuilderService.Verify();
            }
        }
    }
}
