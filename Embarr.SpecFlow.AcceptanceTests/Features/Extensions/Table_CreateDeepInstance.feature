﻿Feature: Table_CreateDeepInstance
	In order to create an object graph from my gherkin text
	As a dev in test
	I want to be able to create a deep object graph from a simple property syntax

Scenario: Create deep model successfully
	When the following deep properties are hydrated:
	| property                   | value                |
	| Name                       | My Index 1           |
	| Dec                        | 1.2                  |
	| NullDec                    | 1.2                  |
	| Doub                       | 0.8                  |
	| Child.Name                 | A name               |
	| Child.Dec                  | 3.2                  |
	| Strings[0]                 | String 1             |
	| Strings[1]                 | String 2             |
	| Ints[0]                    | 123                  |
	| Ints[1]                    | 234                  |
	| Dic["MyKey"].Name          | My Value             |
	| Dic["MyKey3"].Name         | My Value 3           |
	| Coll[0].Dic["MyKey1"].Name | My Deeper Dictionary |
	| Coll[0].Name               | My Coll 0 Name       |
	| Coll[0].Dec                | 3.4                  |
	| Coll[1].Name               | My Coll 1 Name       |
	| Coll[1].Dec                | 4.5                  |
	| Coll[1].Coll[0].Dec        | 10.99                |
	| Child.Coll[0].Dec          | 11.99                |
	| Child.Coll[0].Coll[0].Name | V.Deep               |
	| MyArray[0].Name            | Array 1              |
	| MyArray[0].Doub            | 12.34                |
	| MyArray[1].Name            | Array 2              |
	| MyArray[1].Doub            | 56.78                |
	| MyIntArray[0]              | 123                  |
	| MyIntArray[1]              | 234                  |
	| MyStringArray[0]           | MyStr1               |
	| MyStringArray[1]           | MyStr2               |	

	Then the following properties shoud be populated:
	| property                   | value                |
	| Name                       | My Index 1           |
	| Dec                        | 1.2                  |
	| NullDec                    | 1.2                  |
	| Doub                       | 0.8                  |
	| Child.Name                 | A name               |
	| Child.Dec                  | 3.2                  |
	| Strings[0]                 | String 1             |
	| Strings[1]                 | String 2             |
	| Ints[0]                    | 123                  |
	| Ints[1]                    | 234                  |
	| Dic["MyKey"].Name          | My Value             |
	| Dic["MyKey3"].Name         | My Value 3           |
	| Coll[0].Dic["MyKey1"].Name | My Deeper Dictionary |
	| Coll[0].Name               | My Coll 0 Name       |
	| Coll[0].Dec                | 3.4                  |
	| Coll[1].Name               | My Coll 1 Name       |
	| Coll[1].Dec                | 4.5                  |
	| Coll[1].Coll[0].Dec        | 10.99                |
	| Child.Coll[0].Dec          | 11.99                |
	| Child.Coll[0].Coll[0].Name | V.Deep               |
	| MyArray[0].Name            | Array 1              |
	| MyArray[0].Doub            | 12.34                |
	| MyArray[1].Name            | Array 2              |
	| MyArray[1].Doub            | 56.78                |
	| MyIntArray[0]              | 123                  |
	| MyIntArray[1]              | 234                  |
	| MyStringArray[0]           | MyStr1               |
	| MyStringArray[1]           | MyStr2               |