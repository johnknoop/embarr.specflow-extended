﻿using System;

namespace Embarr.SpecFlow.Extensions.Services
{
    public interface ISimpleTypeService
    {
        object GetSimpleTypeValue(string value, Type type);
    }
}