﻿using System;
using System.Collections.Generic;
using Embarr.SpecFlow.DbC;
using Embarr.SpecFlow.Extensions.Models;

namespace Embarr.SpecFlow.Extensions.Services
{
    public class TableExtensionsService
    {
        private readonly IPropertyBuilderService propertyBuilderService;

        public TableExtensionsService()
        {
            propertyBuilderService = new PropertyBuilderService();
        }

        public TableExtensionsService(IPropertyBuilderService propertyBuilderService)
        {
            this.propertyBuilderService = propertyBuilderService;
        }

        public T CreateDeepInstance<T>(List<Property> properties)
        {
            Condition.Requires<ArgumentException>(properties != null, "CreateDeepInstance properties cannot be null");

            var instance = Activator.CreateInstance<T>();

            foreach (var property in properties)
            {
                propertyBuilderService.BuildProperty(property, 0, instance);
            }
            
            return instance;
        }
    }
}