﻿using System;
using System.Collections;
using System.Collections.Generic;
using Embarr.SpecFlow.Extensions.Models;

namespace Embarr.SpecFlow.Extensions.Services
{
    public static class PropertyInstanceService
    {
        internal static object GetInstancePropertyValue(Property property, int partIndex, object parent)
        {
            var propertyPart = property.Parts[partIndex];
            var propertyInfo = parent.GetType().GetProperty(propertyPart.Name);

            var propertyValue = propertyInfo.GetValue(parent);

            if (partIndex == property.Parts.Count - 1 && propertyPart.Type == PropertyType.Standard)
            {
                return propertyValue;
            }

            if (partIndex == property.Parts.Count - 1 && propertyPart.Type == PropertyType.KeyedOrIndexer)
            {
                if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IList<>).FullName).Length > 0)
                {
                    return ((IList)propertyValue)[int.Parse(propertyPart.Key)];
                }

                if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IDictionary<,>).FullName).Length > 0)
                {
					var key = GetDictionaryKey(propertyValue, propertyPart);
					return ((IDictionary)propertyValue)[key];
                }
            }
            else if (partIndex < property.Parts.Count - 1 && propertyPart.Type == PropertyType.KeyedOrIndexer)
            {
                if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IList<>).FullName).Length > 0)
                {
                    return GetInstancePropertyValue(property, partIndex + 1, ((IList)propertyValue)[int.Parse(propertyPart.Key)]);
                }

                if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IDictionary<,>).FullName).Length > 0)
                {
	                var key = GetDictionaryKey(propertyValue, propertyPart);
	                return GetInstancePropertyValue(property, partIndex + 1, ((IDictionary)propertyValue)[key]);
                }
            }

            return GetInstancePropertyValue(property, partIndex + 1, propertyValue);
        }

	    private static object GetDictionaryKey(object propertyValue, PropertyPart propertyPart)
	    {
		    var dictionary = ((IDictionary) propertyValue);

		    var arguments = dictionary.GetType().GetGenericArguments();
		    var keyType = arguments[0];

		    var key = Convert.ChangeType(propertyPart.Key, keyType);
		    return key;
	    }

	    private static bool MyInterfaceFilter(Type typeObj, Object criteriaObj)
        {
            if (typeObj.ToString().Contains(criteriaObj.ToString()))
            {
                return true;
            }

            return false;
        }
    }
}
