﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Embarr.SpecFlow.DbC;
using Embarr.SpecFlow.Extensions.Models;

namespace Embarr.SpecFlow.Extensions.Services
{
    public class PropertyBuilderService : IPropertyBuilderService
    {
        private readonly ISimpleTypeService simpleTypeService;

        public PropertyBuilderService()
        {
            simpleTypeService = new SimpleTypeService();
        }

        public PropertyBuilderService(ISimpleTypeService simpleTypeService)
        {
            this.simpleTypeService = simpleTypeService;
        }

        public void BuildProperty(Property property, int partIndex, object parent)
        {
            Condition.Requires<ArgumentException>(
                partIndex <= property.Parts.Count - 1,
                "Please check '" + property.PropertyPath + "'. You might be trying to set a complex object rather than a primitive/simple item from the complex object");

            var propertyInfo = parent.GetType().GetProperty(property.Parts[partIndex].Name);

            Condition.Assert<ArgumentException>(propertyInfo != null, "The model '" + parent.GetType().Name + "' does not appear to contain a property called '" + property.Parts[partIndex].Name + "'");

            if (!propertyInfo.CanWrite)
            {
                return;
            }

            if (IsSimpleType(propertyInfo))
            {
                SetSimpleTypeProperty(property, parent, propertyInfo);
                return;
            }

            if (IsComplexType(property, partIndex))
            {
                BuildComplexChildProperty(property, partIndex, parent, propertyInfo);
                return;
            }

            if (propertyInfo.PropertyType.IsArray)
            {
                BuildArrayProperty(property, partIndex, parent, propertyInfo);
                return;
            }

            if (IsGenericListProperty(propertyInfo))
            {
                BuildGenericListProperty(property, partIndex, parent, propertyInfo);
                return;
            }

            if (IsGenericDictionaryProperty(propertyInfo))
            {
                BuildDictionaryProperty(property, partIndex, parent, propertyInfo);
            }
        }

        private void BuildDictionaryProperty(Property property, int partIndex, object parent, PropertyInfo propertyInfo)
        {
            var childInstance = propertyInfo.GetValue(parent);
            var genericTypeKeyArgument = propertyInfo.PropertyType.GenericTypeArguments[0];
            var genericTypeValueArgument = propertyInfo.PropertyType.GenericTypeArguments[1];
            if (childInstance == null)
            {
                var dictionaryType = typeof (Dictionary<,>);
                var constructedListType = dictionaryType.MakeGenericType(genericTypeKeyArgument, genericTypeValueArgument);
                childInstance = Activator.CreateInstance(constructedListType);
                propertyInfo.SetValue(parent, childInstance);
            }

            if (IsSimpleType(genericTypeValueArgument))
            {
                BuildSimpleTypeDictionaryItem(property, partIndex, childInstance, genericTypeKeyArgument,
                    genericTypeValueArgument);
            }
            else
            {
                BuildComplexTypeDictionaryItem(property, partIndex, childInstance, genericTypeValueArgument,
                    genericTypeKeyArgument);
            }
        }

        private void BuildComplexTypeDictionaryItem(Property property, int partIndex, object childInstance,
            Type genericTypeValueArgument, Type genericTypeKeyArgument)
        {
            var key = simpleTypeService.GetSimpleTypeValue(property.Parts[partIndex].Key, genericTypeKeyArgument);
            var item = ((IDictionary) childInstance)[key];

            if (item == null)
            {
                item = Activator.CreateInstance(genericTypeValueArgument);
                ((IDictionary)childInstance).Add(key, item);
            }

            BuildProperty(property, partIndex + 1, item);
        }

        private void BuildSimpleTypeDictionaryItem(Property property, int partIndex, object childInstance,
            Type genericTypeKeyArgument, Type genericTypeValueArgument)
        {
            ((IDictionary) childInstance).Add(
                simpleTypeService.GetSimpleTypeValue(property.Parts[partIndex].Key, genericTypeKeyArgument),
                simpleTypeService.GetSimpleTypeValue(property.RawStringValue, genericTypeValueArgument));
        }

        private static bool IsGenericDictionaryProperty(PropertyInfo propertyInfo)
        {
            return propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IDictionary<,>).FullName).Length > 0;
        }

        private static bool IsComplexType(Property property, int partIndex)
        {
            return property.Parts[partIndex].Type == PropertyType.Standard;
        }

        private void BuildGenericListProperty(Property property, int partIndex, object parent, PropertyInfo propertyInfo)
        {
            var childInstance = propertyInfo.GetValue(parent);
            var genericTypeArgument = propertyInfo.PropertyType.GenericTypeArguments[0];
            if (childInstance == null)
            {
                var listType = typeof (List<>);
                var constructedListType = listType.MakeGenericType(genericTypeArgument);
                childInstance = Activator.CreateInstance(constructedListType);
                propertyInfo.SetValue(parent, childInstance);
            }

            if (IsSimpleType(genericTypeArgument))
            {
                BuildSimpleListItemProperty(property, childInstance, genericTypeArgument);
            }
            else
            {
                BuildComplexListItemProperty(property, partIndex, childInstance, genericTypeArgument);
            }
        }

        private void BuildSimpleListItemProperty(Property property, object childInstance, Type genericTypeArgument)
        {
            ((IList) childInstance).Add(simpleTypeService.GetSimpleTypeValue(property.RawStringValue, genericTypeArgument));
        }

        private void BuildComplexListItemProperty(Property property, int partIndex, object childInstance,
            Type genericTypeArgument)
        {
            var indexPosition = int.Parse(property.Parts[partIndex].Key);
            object item;
            if (((IList) childInstance).Count > indexPosition)
            {
                item = ((IList) childInstance)[indexPosition];
            }
            else
            {
                item = Activator.CreateInstance(genericTypeArgument);
                ((IList) childInstance).Add(item);
            }

            BuildProperty(property, partIndex + 1, item);
        }

        private static bool IsGenericListProperty(PropertyInfo propertyInfo)
        {
            return propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IList<>).FullName).Length > 0;
        }

        private void BuildArrayProperty(Property property, int partIndex, object parent, PropertyInfo propertyInfo)
        {
            var propertyElementType = propertyInfo.PropertyType.GetElementType();

            var childInstance = (IList) propertyInfo.GetValue(parent);

            if (childInstance == null)
            {
                childInstance = Array.CreateInstance(propertyElementType, 0);
                propertyInfo.SetValue(parent, childInstance);
            }

            if (IsSimpleType(propertyElementType))
            {
                BuildSimpleTypeArrayItem(property, parent, propertyElementType, childInstance, propertyInfo);
            }
            else
            {
                BuildComplexTypeArrayItem(property, partIndex, parent, childInstance, propertyElementType, propertyInfo);
            }
        }

        private void BuildComplexTypeArrayItem(Property property, int partIndex, object parent, IList childInstance,
            Type propertyElementType, PropertyInfo propertyInfo)
        {
            var indexPosition = int.Parse(property.Parts[partIndex].Key);
            object item;
            if (childInstance.Count > indexPosition)
            {
                item = childInstance[indexPosition];
            }
            else
            {
                item = Activator.CreateInstance(propertyElementType);

                Array newArray = Array.CreateInstance(propertyElementType, childInstance.Count + 1);
                Array.Copy((Array) childInstance, newArray, childInstance.Count);
                childInstance = newArray;
                childInstance[childInstance.Count - 1] = item;
            }

            propertyInfo.SetValue(parent, childInstance);
            BuildProperty(property, partIndex + 1, item);
        }

        private void BuildSimpleTypeArrayItem(Property property, object parent, Type propertyElementType, IList childInstance,
            PropertyInfo propertyInfo)
        {
            Array newArray = Array.CreateInstance(propertyElementType, childInstance.Count + 1);
            Array.Copy((Array) childInstance, newArray, childInstance.Count);
            childInstance = newArray;
            childInstance[childInstance.Count - 1] = simpleTypeService.GetSimpleTypeValue(property.RawStringValue,
                propertyElementType);
            propertyInfo.SetValue(parent, childInstance);
        }

        private void BuildComplexChildProperty(Property property, int partIndex, object parent, PropertyInfo propertyInfo)
        {
            var childInstance = propertyInfo.GetValue(parent);
            if (childInstance == null)
            {
                childInstance = Activator.CreateInstance(propertyInfo.PropertyType);
                propertyInfo.SetValue(parent, childInstance);
            }

            BuildProperty(property, partIndex + 1, childInstance);
        }

        private void SetSimpleTypeProperty(Property property, object parent, PropertyInfo propertyInfo)
        {
            var simpleTypeValue = simpleTypeService.GetSimpleTypeValue(property.RawStringValue, propertyInfo.PropertyType);
            propertyInfo.SetValue(parent, simpleTypeValue);
        }

        private static bool MyInterfaceFilter(Type typeObj, Object criteriaObj)
        {
            if (typeObj.ToString().Contains(criteriaObj.ToString()))
            {
                return true;
            }

            return false;
        }

        private static bool IsSimpleType(PropertyInfo property)
        {
            if (property == null)
            {
                return false;
            }

            return property.PropertyType.IsPrimitive ||
                   property.PropertyType.IsValueType ||
                   property.PropertyType == typeof(string) ||
                   property.PropertyType.FullName.Contains("Nullable");
        }

        private static bool IsSimpleType(Type type)
        {
            if (type == null)
            {
                return false;
            }

            return type.IsPrimitive ||
                   type.IsValueType ||
                   type == typeof(string) ||
                   type.FullName.Contains("Nullable");
        }
    }
}