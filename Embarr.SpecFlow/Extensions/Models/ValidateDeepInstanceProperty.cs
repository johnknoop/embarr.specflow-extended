﻿namespace Embarr.SpecFlow.Extensions.Models
{
    public class ValidateDeepInstanceProperty
    {
        public string Path { get; set; }

        public string ActualValue { get; set; }
        
        public string ExpectedValue { get; set; }

        public bool IsMatch
        {
            get { return ActualValue == ExpectedValue; }
        }
    }
}