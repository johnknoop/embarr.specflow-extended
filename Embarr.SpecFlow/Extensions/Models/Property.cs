﻿using System.Collections.Generic;

namespace Embarr.SpecFlow.Extensions.Models
{
    public class Property
    {
        public Property()
        {
            Parts = new List<PropertyPart>();
        }

        public string PropertyPath { get; set; }

        public List<PropertyPart> Parts { get; set; }

        public string RawStringValue { get; set; }
    }
}
