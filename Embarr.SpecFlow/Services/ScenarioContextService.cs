﻿using TechTalk.SpecFlow;

namespace Embarr.SpecFlow.Services
{
    /// <summary>
    /// Service for accessing the SpecFlow ScenarioContext
    /// </summary>
    public class ScenarioContextService : ContextService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScenarioContextService"/> class.
        /// </summary>
        public ScenarioContextService()
            : base(ScenarioContext.Current)
        {

        }
    }
}
