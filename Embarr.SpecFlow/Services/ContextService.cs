﻿using System.Collections.Generic;

namespace Embarr.SpecFlow.Services
{
    /// <summary>
    /// Abstract context class.
    /// </summary>
    public abstract class ContextService
    {
        private readonly Dictionary<string, object> context;

        protected ContextService(Dictionary<string, object> context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public virtual Dictionary<string, object> Context
        {
            get { return context; }
        }

        /// <summary>
        /// Saves the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        public virtual void SaveValue<T>(T value)
        {
            var key = typeof(T).FullName;
            SaveValue(key, value);
        }

        /// <summary>
        /// Saves the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public virtual void SaveValue<T>(string key, T value)
        {
            if (context.ContainsKey(key))
            {
                context[key] = value;
            }
            else
            {
                context.Add(key, value);
            }
        }

        /// <summary>
        /// Saves to list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        public virtual void SaveToList<T>(T value)
        {
            var key = typeof(T).FullName;
            SaveToList("list_" + key, value);
        }

        /// <summary>
        /// Saves to list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public virtual void SaveToList<T>(string key, T value)
        {
            var list = GetValue<List<T>>(key) ?? new List<T>();
            list.Add(value);
            SaveValue(key, list);
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual T GetValue<T>()
        {
            var key = typeof(T).FullName;

            return GetValue<T>(key);
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public virtual T GetValue<T>(string key)
        {
            if (!context.ContainsKey(key))
            {
                return default(T);
            }

            return (T)context[key];
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual List<T> GetList<T>()
        {
            var key = typeof(T).FullName;

            return GetList<T>("list_" + key);
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public virtual List<T> GetList<T>(string key)
        {
            var list = GetValue<List<T>>(key) ?? new List<T>();
            return list;
        }

        /// <summary>
        /// Clears the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public virtual void ClearValue<T>()
        {
            var key = typeof(T).FullName;
            ClearValue(key);
        }

        /// <summary>
        /// Clears the value.
        /// </summary>
        /// <param name="key">The key.</param>
        public virtual void ClearValue(string key)
        {
            if (context.ContainsKey(key))
            {
                context.Remove(key);
            }
        }
    }
}
