﻿using System;

namespace Embarr.SpecFlow.DbC
{
    public static class Condition
    {
        public static void Requires<TException>(bool condition, string message) where TException : Exception
        {
            if (!condition)
            {
                var exception = (TException)Activator.CreateInstance(typeof (TException), message);
                throw exception;
            }
        }

        public static void Assert<TException>(bool condition, string message) where TException : Exception
        {
            if (!condition)
            {
                var exception = (TException)Activator.CreateInstance(typeof(TException), message);
                throw exception;
            }
        }
    }
}
